import subprocess


def getDeps(name):

    rawDeps = subprocess.run(['sudo', 'dnf', 'repoquery', '--requires', '--recursive', '--resolve', name],
                             capture_output=True, universal_newlines=True)
    rawDeps = rawDeps.stdout.split("\n")

    output = []

    for line in rawDeps:
        line = line[0:line.rfind("-")]
        line = line[0:line.rfind("-")]

        output.append(line)

    return output
