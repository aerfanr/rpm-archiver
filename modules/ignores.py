def add_ignore(name):
    file = open("RPMIgnores", 'a')

    file.write(name + "\n\r")


def list_ignores():
    file = open("RPMIgnores", 'r')

    print(file.readlines())


def remove_ignore(name):
    with open("RPMIgnores", 'r') as file:
        lines = file.readlines()

    with open("RPMIgnores", 'w') as file:
        for line in lines:
            if line.strip('\n\r') != name:
                file.write(line)


def add_list(name, path):
    with open(path[0:path.rfind("/")] + "/groups/" + name) as file:
        lines = file.readlines()

    with open("RPMIgnores", 'a') as file:
        for line in lines:
            file.write(line)
