import argparse
import os
from modules.getDeps import getDeps
from modules.ignores import add_ignore, list_ignores, remove_ignore, add_list

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="RPM Archiver | "
                                                 "This is a python CLI tool that helps you to download RPMs and create "
                                                 "archives from them. This should be useful when you are trying to "
                                                 "download packages that you want to install on an offline system.")

    parser.add_argument("-p", "--directory", type=str, default=os.getcwd(), help="Set the directory where RPMs are "
                                                                                 "stored. Default is current directory."
                        )
    parser.add_argument("-d", "--download", type=str, help="Download package with all dependencies except installed or "
                                                           "ignored ones.")
    parser.add_argument("-i", "--ignore", type=str, help="Add a package to ignore list.")
    parser.add_argument("-l", "--list-ignores", action="store_true", help="List ignored packages")
    parser.add_argument("-r", "--remove-ignore", type=str, help="Remove package from ignore list.")
    parser.add_argument("-a", "--add-list", type=str, help="Add a predefined list to ignore list.")

    args = parser.parse_args()
    directory = args.directory

    print()

    if args.download is not None:
        print("---Finding package and dependencies")

        print(getDeps(args.download))

    elif args.ignore is not None:
        print("---Adding ignore")

        add_ignore(args.ignore)

        print("---Done")

    elif args.list_ignores is True:
        print("---Listing started")

        list_ignores()

        print("---End of list")

    elif args.remove_ignore is not None:
        print("---Removing ignore")

        remove_ignore(args.remove_ignore)

        print("---Ignore removed")

    elif args.add_list is not None:
        print("---Adding list")

        add_list(args.add_list, os.path.realpath(__file__))

        print("---Done")

    else:
        print("Not valid")

    print()
