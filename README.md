# RPM Archiver

This is a python CLI tool that helps you to download RPMs and create archives from them.
This should be useful when you are trying to download packages that you want to install on an offline system.

This project is discontinued. Instead, see [RPM Offliner](gitlab.com/aerfanr/rpm-offliner)